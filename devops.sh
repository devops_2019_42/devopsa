#!/usr/bin/env bash


LOGFILE="$(pwd)/devops.log"
mkdir -p datas/gitlab/{config,logs,datas} 2&>/dev/null

if [ ! -d "$(pwd)/datas/back" ]; then
    echo "Please download the backend application (backend.zip)"
    exit 1
fi

if [ ! -d "$(pwd)/datas/front" ]; then
    echo "Please download the frontend application (frontend.zip)"
    exit 1
fi
echo -e "\n 🦊 Starting Gitlab CE service\n"
docker run --detach \
  --hostname localhost \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $(pwd)/datas/gitlab/config:/etc/gitlab \
  --volume $(pwd)/datas/gitlab/logs:/var/log/gitlab \
  --volume $(pwd)/datas/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest &>>$LOGFILE || docker start gitlab &>>$LOGFILE
echo -e "\n 🐘 Setting up back PHP composer app locally\n"
cd datas/back && cp .env.example .env 2&>/dev/null || echo "" && \
    composer update &>>$LOGFILE && \
    yarn install &>>$LOGFILE && php artisan config:clear &>>$LOGFILE && \
    php artisan key:generate &>>$LOGFILE && \
    echo -e "\n 🚦 Running back TA\n"
    ./vendor/bin/phpunit 
echo -e "\n 🐘 Setting up front Angular app locally\n"
cd - && cd datas/front && yarn install &>>$LOGFILE && \
    echo -e "\n 🚦 Running front TA\n" && \
    yarn test